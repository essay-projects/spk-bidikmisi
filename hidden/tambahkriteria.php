<?php 
          if (!empty($_POST['Tambah'])) {
            // print_r( $_POST["id"]);
            // print_r( $_POST["subkriteria"]);
            // print_r( $_POST["jenis"]);
            // // exit;   
            $id = $_POST["id"];
            $subkriteria = $_POST["subkriteria"];
            $jenis = $_POST["jenis"];

            include_once("koneksi.php");

            $result= mysqli_query($connect , "INSERT INTO subkriteria VALUES ('', '$id','$subkriteria','$jenis')" );
            exit(header("location: /bidikmisi/jeniskriteria.php"));

        }
    ?>

<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark"></h1>

            <div class="card" style="margin-top: 50px">
            <div class="card-header">
              <h3 class="card-title">Tambah Jenis Kriteria</h3>

              <?php include "koneksi.php"; ?>

              <form style="margin-top: 50px;" method="POST">
                <?php
                  $sql = "SELECT id, aspek_kriteria FROM kriteria";
                  $result = $connect->query($sql);
                ?>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Aspek Kriteria</label>
                <div class="col-sm-10">
                    <select class="form-control" name="id" >
                      <option selected="selected">--</option>
                      <?php
                        foreach($result as $values) { 
                      ?>
                          <option value="<?= $values['id'] ?>"><?= $values['aspek_kriteria'] ?></option>
                      <?php
                        }
                      ?>
                     
                    </select>
                </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Subkriteria</label>
                <div class="col-sm-10">
                    <input type="" class="form-control" name="subkriteria" >
                </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-sm-2 col-form-label">Jenis Kriteria</label>
                <div class="col-sm-10">
                    <select class="form-control" name="jenis">
                              <option>--</option>
                              <option value="Cost">Cost</option>
                              <option value="Benefit">Benefit</option>
                    </select>
                </div>
                </div>

    <div class="form-group row">
      <div class="col-sm-10">
        <button type="submit" name="Tambah" value="Add" class="btn btn-primary">Tambah</button>
      </div>
    </div>

    
</form>