<?php
	$page = "Perhitungan";
?>
<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark"></h1>

            <div class="card" style="margin-top: 50px">
            <div class="card-header">

             <div id="tab">
             <h3 class="card-title">Tabel Hasil Perhitungan </h3>
                <table class="table-responsive table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
                    <tr>
                      <th rowspan="2">No</th>
                      <th rowspan="2" style="text-align: center; font-size: 10pt">Nama Peserta</th>
                      <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Orangtua</th>
                      <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Mahasiswa</th>
                      <th colspan="6" style="text-align: center; font-size: 10pt">Aspek Kondisi Rumah</th> 
                      <th rowspan="2">Hasil</th>
                    </tr>
                    <tr>
                      <?php include "koneksi.php"; ?>
                      <?php 
                        $sql = "SELECT id_kriteria, sub_kriteria FROM subkriteria";
                        $result = $connect->query($sql);
                       ?>
                       <?php 
                       foreach($result as $value) {?>
                       <td style="font-size: 9pt"><?= $value['sub_kriteria']?></td>
                       <?php 
                        }
                        ?>
                    </tr>

                    <?php 
                    include "koneksi.php";
                    $peserta = mysqli_query($connect, 'SELECT * FROM peserta');
                    while ($isi = mysqli_fetch_array($peserta)) {
                    $total = 0; ?>
                    <tr>
                      <td><?= $isi["id"]?></td>
                      <td><?= $isi["nama"]?></td>
                     <?php
                      $jenis = mysqli_query($connect, "SELECT * FROM subkriteria ORDER BY id ASC");
                      while ($jeniskriteria = mysqli_fetch_array($jenis)) {
                      ?>
                      <?php 	
                      $hnormalisasi = mysqli_query($connect , "SELECT hasilnormalisasi.nilai,kriteria.bobot FROM hasilnormalisasi INNER JOIN kriteria ON hasilnormalisasi.id_kriteria=kriteria.id WHERE id_peserta='{$isi["id"]}' AND id_subkriteria='{$jeniskriteria["id"]}' limit 1");
                      $hnormalisasi = mysqli_fetch_array($hnormalisasi);
                      $hasil = $hnormalisasi["nilai"] * $hnormalisasi["bobot"];
                      $total = $total + $hasil ;
                       ?>
                    <td><?= $hasil ?></td>

                    <?php 
                    }
                    $data = mysqli_query($connect, "SELECT id FROM hitung WHERE id_peserta='{$isi["id"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE hitung SET hasil = '{$total}' WHERE id_peserta='{$isi["id"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO hitung VALUES ('','{$isi["id"]}','{$total}')");
                            }	 
                    ?>
                    <td><?= $total ?></td>
                    <?php 	
                    } 
                    ?>
                 </table>
             	 </div>
                 <p>
        				<input type="button" value="Export PDF" id="btPrint" onclick="createPDF()" />
    			</p>
		<script>
    		function createPDF() {
        var sTable = document.getElementById('tab').innerHTML;

        var style = "<style>";
        style = style + "table {width: 100%;font: 17px Calibri;}";
        style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
        style = style + "padding: 2px 3px;text-align: center;}";
        style = style + "</style>";

        // CREATE A WINDOW OBJECT.
        var win = window.open('', '', 'height=700,width=700');

        win.document.write('<html><head>');
        win.document.write('<title>Hasil Perhitungan Metode SAW</title>');   // <title> FOR PDF HEADER.
        win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
        win.document.write('</head>');
        win.document.write('<body>');
        win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
        win.document.write('</body></html>');

        win.document.close(); 	// CLOSE THE CURRENT WINDOW.

        win.print();    // PRINT THE CONTENTS.
    }
	</script>
  <div class="form-group row">
  <a href="perangkingan.php">
  <div class="col-sm-10">
  <button type="submit" name="Tambah" value="Add" class="btn btn-primary">Hasil Perangkingan</button>
  </div>
  </a>
  </div>

   <div class="card-body">
            </div>
          </div>
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>

  <?php 
  include_once 'footer.php';
   ?>