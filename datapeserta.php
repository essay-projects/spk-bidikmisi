<?php
	$page = "Data Peserta";
?>

<?php 
                      include "koneksi.php";
                      if (!empty($_POST["Submit"])) {
                        if($_FILES['csv']['size'] > 0){
                          $file = $_FILES['csv']['tmp_name'];
                          $handle = fopen($file,"r");
                          $i = 0;
                          while (($data = fgetcsv($handle, 1000, ";")) !==FALSE) {
                           {
                            // $data[5] = floatval($data[5]);
                            // print_r($data[5]);
                            // die;
                            // $data[6] = floatval($data[6]);
                            $query= "INSERT INTO peserta(nama,penghasilan,pendidikan,pekerjaan,tanggungan,nilai_un,nilai_ijazah,p_akademik,p_nonakademik,l_rumah,l_tanah,listrik,pdam,pbb,status_rumah) VALUES('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]',$data[5],$data[6],'$data[7]','$data[8]','$data[9]','$data[10]','$data[11]','$data[12]','$data[13]','$data[14]')";
                            $connect->query($query);
                            }
                          }
                        } exit(header("location: /bidikmisi/datapeserta.php"));               }

                     ?>

<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark"></h1>

          <div class="card" style="margin-top: 50px">
            <div class="card-header">

              <h3 class="card-title">Tabel Daftar Calon Peserta </h3>

              <form action="" method="post" enctype="multipart/form-data" style="margin-top: 30px;">
                Choose your file:
                <input name="csv" type="file" id="csv" />
                <br>
                <input type="submit" name="Submit" value="Submit" />


              </form>


              <table class="table-responsive table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
                <tr>
                  <th rowspan="2">No</th>
                  <th rowspan="2" style="text-align: center; font-size: 10pt">Nama Peserta</th>
                  <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Orangtua</th>
                  <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Mahasiswa</th>
                  <th colspan="6" style="text-align: center; font-size: 10pt">Aspek Kondisi Rumah</th>
                </tr>
                <tr>
                  <td style="font-size: 9pt">Penghasilan</td>
                  <td style="font-size: 9pt">Pendidikan</td>
                  <td style="font-size: 9pt">Pekerjaan</td>
                  <td style="font-size: 9pt">Jumlah Tanggungan</td>
                  <td style="font-size: 9pt">Nilai UN</td>
                  <td style="font-size: 9pt">Nilai Ijazah</td>
                  <td style="font-size: 9pt">Prestasi Akademik</td>
                  <td style="font-size: 9pt">Prestasi non-Akademik</td>
                  <td style="font-size: 9pt">Luas Rumah</td>
                  <td style="font-size: 9pt">Luas Tanah</td>
                  <td style="font-size: 9pt">Rekening Listrik/Bln</td>
                  <td style="font-size: 9pt">PDAM</td>
                  <td style="font-size: 9pt">PBB/Thn</td>
                  <td style="font-size: 9pt">Kepemilikan Rumah</td>
                </tr>

                <?php   
                      include "koneksi.php";
                      $peserta = mysqli_query($connect, 'SELECT * FROM peserta');
                  while ($isi = mysqli_fetch_array($peserta)) { ?>

                <tr>
                  <td><?= 1 + @$i++ ?>.</td>
                  <td><?= $isi["nama"]?></td>
                  <td><?= $isi["penghasilan"]?></td>
                  <td><?= $isi["pendidikan"]?></td>
                  <td><?= $isi["pekerjaan"]?></td>
                  <td><?= $isi["tanggungan"]?></td>
                  <td><?= $isi["nilai_un"]?></td>
                  <td><?= $isi["nilai_ijazah"]?></td>
                  <td><?= $isi["p_akademik"]?></td>
                  <td><?= $isi["p_nonakademik"]?></td>
                  <td><?= $isi["l_rumah"]?></td>
                  <td><?= $isi["l_tanah"]?></td>
                  <td><?= $isi["listrik"]?></td>
                  <td><?= $isi["pdam"]?></td>
                  <td><?= $isi["pbb"]?></td>
                  <td><?= $isi["status_rumah"]?></td>

                </tr><?php


                  }

                     ?>

              </table>

              <!-- <button type="button" class=" col-sm-2 btn btn-block btn-info float-left">Simpan Data</button> -->
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">

                </table>
              </div>
              <!-- /.card-body -->
            </div>

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>
  <!-- /.content-header -->

  <!-- Main content -->

  <?php 
  include_once 'footer.php';
   ?>