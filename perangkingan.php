<?php
	$page = "Hasil";
?>
<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark"></h1>

            <div class="card" style="margin-top: 50px">
            <div class="card-header">

             <div id="tab">
             <h3 class="card-title">Tabel Hasil Perangkingan </h3>
             <table class="table table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
              <tr>
                <td>No</td>  
                <td style="text-align: center;">Nama Peserta</td>
                <td style="text-align: center;">Hasil Perhitungan Metode SAW</td>
              </tr>

             
              <?php   
              include "koneksi.php";
              $peserta = mysqli_query($connect, 'SELECT hitung.hasil,peserta.nama FROM hitung INNER JOIN peserta ON hitung.id_peserta=peserta.id ORDER BY hasil DESC');
                    while ($isi = mysqli_fetch_array($peserta)) {

              $nama = $isi["nama"];
              $hasil = $isi["hasil"];
              ?>
              <tr>
              <td><?= 1 + @$i++ ?></td>
              <td><?= $nama ?></td>
              <td><?= $hasil ?></td>
              </tr>
               <?php    
                }
              ?>
            </table>
          </div>

           <p>
                <input type="button" value="Export PDF" id="btPrint" onclick="createPDF()" />
          </p>
    <script>
        function createPDF() {
        var sTable = document.getElementById('tab').innerHTML;

        var style = "<style>";
        style = style + "table {width: 100%;font: 17px Calibri;}";
        style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
        style = style + "padding: 2px 3px;text-align: center;}";
        style = style + "</style>";

        // CREATE A WINDOW OBJECT.
        var win = window.open('', '', 'height=700,width=700');

        win.document.write('<html><head>');
        win.document.write('<title>Hasil Perangkingan Metode SAW</title>');   // <title> FOR PDF HEADER.
        win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
        win.document.write('</head>');
        win.document.write('<body>');
        win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
        win.document.write('</body></html>');

        win.document.close();   // CLOSE THE CURRENT WINDOW.

        win.print();    // PRINT THE CONTENTS.
    }
  </script>

   <div class="card-body">
            </div>
          </div>
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>

  <?php 
  include_once 'footer.php';
   ?>