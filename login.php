<?php 
    include_once 'header.php';
  ?>

    <div class="login-box">
        <div class="login-logo">
            <img src="30.png" alt="">
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <form method="post" action="cek_login.php">
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12" style="text-align: center; align-content: center;">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
        <p class="login-box-msg">
                <?php 
	                        if(isset($_GET['pesan'])){
	                    	if($_GET['pesan'] == "gagal"){
			                    echo "Login gagal! username dan password salah!";
		                        }else if($_GET['pesan'] == "logout"){
			                        echo "Anda telah berhasil logout";
		                            }else if($_GET['pesan'] == "belum_login"){
			                            echo "Anda harus login untuk mengakses halaman admin";
		                        }
                        	}
                         ?>
                </p>
    </div>
    <!-- /.login-box -->

    <?php 
  include_once 'footer.php';
   ?>