<?php
	$page = "Normalisasi";
?>
<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark"></h1>

          <div class="card" style="margin-top: 50px">
            <div class="card-header">

              <h3 class="card-title">Tabel Rating Kecocokan setiap Alternatif </h3>

              <table class="table-responsive table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
                <tr>
                  <th rowspan="2">No</th>
                  <th rowspan="2" style="text-align: center; font-size: 10pt">Nama Peserta</th>
                  <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Orangtua</th>
                  <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Mahasiswa</th>
                  <th colspan="6" style="text-align: center; font-size: 10pt">Aspek Kondisi Rumah</th>
                </tr>
                <tr>
                  <?php include "koneksi.php"; ?>
                  <?php 
                        $sql = "SELECT id_kriteria, sub_kriteria FROM subkriteria";
                        $result = $connect->query($sql);
                       ?>
                  <?php 
                       foreach($result as $value) {?>
                  <td style="font-size: 9pt"><?= $value['sub_kriteria']?></td>
                  <?php 
                        }
                        ?>

                  <!-- <td style="font-size: 9pt">Penghasilan</td>
                      <td style="font-size: 9pt">Pendidikan</td>
                      <td style="font-size: 9pt">Pekerjaan</td>
                      <td style="font-size: 9pt">Jumlah Tanggungan</td>
                      <td style="font-size: 9pt">Nilai UN</td>
                      <td style="font-size: 9pt">Nilai Ijazah</td>
                      <td style="font-size: 9pt">Prestasi Akademik</td>
                      <td style="font-size: 9pt">Prestasi non-Akademik</td>
                      <td style="font-size: 9pt">Luas Rumah</td>
                      <td style="font-size: 9pt">Luas Tanah</td>
                      <td style="font-size: 9pt">Rekening Listrik/Bln</td>
                      <td style="font-size: 9pt">PDAM</td>
                      <td style="font-size: 9pt">PBB/Thn</td>
                      <td style="font-size: 9pt">Kepemilikan Rumah</td> -->
                </tr>

                <?php   
                      include "koneksi.php";
                      $peserta = mysqli_query($connect, 'SELECT * FROM peserta');
                    while ($isi = mysqli_fetch_array($peserta)) { ?>

                <tr>
                  <td><?= 1 + @$i++ ?>.</td>
                  <td><?= $isi["nama"]?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='1';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['penghasilan']>=$kriterias['min']&& $isi['penghasilan']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>

                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='2';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if (preg_match('/'.$isi["pendidikan"].'/', $kriterias['nama_rating'])){
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                          if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          } 
                      ?></td>
                  <td><?php
                       $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='3';");
                       $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if (preg_match('/'.$isi["pekerjaan"].'/i', str_replace("/", "", $kriterias['nama_rating'])) ){
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='4';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['tanggungan']>=$kriterias['min']&& $isi['tanggungan']<=$kriterias['max']) {
                           $kriteria_data = $kriterias;
                           print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='5';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['nilai_un']>=$kriterias['min']&& $isi['nilai_un']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='6';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['nilai_ijazah']>=$kriterias['min']&& $isi['nilai_ijazah']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='7';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['p_akademik']>=$kriterias['min']&& $isi['p_akademik']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='8';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if (preg_match('/'.$isi["p_nonakademik"].'/', $kriterias['nama_rating'])){
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='9';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['l_rumah']>=$kriterias['min']&& $isi['l_rumah']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php 
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='10';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['l_tanah']>=$kriterias['min']&& $isi['l_tanah']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='11';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['listrik']>=$kriterias['min']&& $isi['listrik']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='12';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['pdam']>=$kriterias['min']&& $isi['pdam']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='13';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if ($isi['pbb']>=$kriterias['min']&& $isi['pbb']<=$kriterias['max']) {
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>
                  <td><?php
                        $kriteria = mysqli_query($connect, "SELECT * FROM rating_kriteria INNER JOIN kriteria ON kriteria.id=rating_kriteria.id_kriteria WHERE id_subkriteria='14';");
                        $kriteria_data = [];
                        while ($kriterias = mysqli_fetch_array($kriteria)) {
                          if (preg_match('/'.$isi["status_rumah"].'/', $kriterias['nama_rating'])){
                            $kriteria_data = $kriterias;
                            print_r($kriterias['rating']);
                          }
                        }
                        if (count($kriteria_data) > 0) {
                            $data = mysqli_query($connect, "SELECT id,id_peserta,id_kriteria,id_subkriteria FROM normalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE normalisasi SET id_rating = '{$kriteria_data["id_rating"]}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$kriteria_data["id_kriteria"]}' AND id_subkriteria='{$kriteria_data["id_subkriteria"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO normalisasi VALUES ('','{$isi["id"]}','{$kriteria_data["id_kriteria"]}','{$kriteria_data["id_subkriteria"]}','{$kriteria_data["id_rating"]}')");
                            }
                          }
                      ?></td>

                </tr><?php


                  }



                     ?>

              </table>
              <div class="form-group row">
                <a href="hitungnormalisasi.php">
                  <div class="col-sm-10">
                    <button type="submit" name="Tambah" value="Add" class="btn btn-primary">Hitung Normalisasi</button>
                  </div>
                </a>
              </div>

              <div class="card-body">
              </div>
            </div>

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>

  <?php 
  include_once 'footer.php';
   ?>