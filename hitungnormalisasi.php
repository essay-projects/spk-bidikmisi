<?php
	$page = "Hasil Normalisasi";
?>
<?php 
    include_once 'header.php';
    include_once 'navbar.php';
    include_once 'sidebar.php';
  ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark"></h1>

            <div class="card" style="margin-top: 50px">
            <div class="card-header">

              <h3 class="card-title">Tabel Hasil Normalisasi </h3>

                <table class="table-responsive table-bordered" style="margin-top: 20px; margin-bottom: 20px;">
                    <tr>
                      <th rowspan="2">No</th>
                      <th rowspan="2" style="text-align: center; font-size: 10pt">Nama Peserta</th>
                      <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Orangtua</th>
                      <th colspan="4" style="text-align: center; font-size: 10pt">Aspek Mahasiswa</th>
                      <th colspan="6" style="text-align: center; font-size: 10pt">Aspek Kondisi Rumah</th> 
                    </tr>
                    <tr>
                      <?php include "koneksi.php"; ?>
                      <?php 
                        $sql = "SELECT id_kriteria, sub_kriteria FROM subkriteria";
                        $result = $connect->query($sql);
                       ?>
                       <?php 
                       foreach($result as $value) {?>
                       <td style="font-size: 9pt"><?= $value['sub_kriteria']?></td>
                       <?php 
                        }
                        ?>
                      <!-- <td style="font-size: 9pt">Penghasilan</td>
                      <td style="font-size: 9pt">Pendidikan</td>
                      <td style="font-size: 9pt">Pekerjaan</td>
                      <td style="font-size: 9pt">Jumlah Tanggungan</td>
                      <td style="font-size: 9pt">Nilai UN</td>
                      <td style="font-size: 9pt">Nilai Ijazah</td>
                      <td style="font-size: 9pt">Prestasi Akademik</td>
                      <td style="font-size: 9pt">Prestasi non-Akademik</td>
                      <td style="font-size: 9pt">Luas Rumah</td>
                      <td style="font-size: 9pt">Luas Tanah</td>
                      <td style="font-size: 9pt">Rekening Listrik/Bln</td>
                      <td style="font-size: 9pt">PDAM</td>
                      <td style="font-size: 9pt">PBB/Thn</td>
                      <td style="font-size: 9pt">Kepemilikan Rumah</td> -->
                    </tr>

                    <?php 
                    include "koneksi.php";
                    $peserta = mysqli_query($connect, 'SELECT * FROM peserta');
                    while ($isi = mysqli_fetch_array($peserta)) { ?>
                    <tr>
                      <td><?= $isi["id"]?></td>
                      <td><?= $isi["nama"]?></td>
                   
                    <?php
                      $jenis = mysqli_query($connect, "SELECT * FROM subkriteria ORDER BY id ASC");
                      while ($jeniskriteria = mysqli_fetch_array($jenis)) { ?>
                        <?php 
                        if ($jeniskriteria['jenis_subkriteria'] == "Benefit") {
                          $benefit = mysqli_query($connect, "SELECT rating_kriteria.rating as nilai_rating FROM normalisasi INNER JOIN rating_kriteria ON normalisasi.id_rating=rating_kriteria.id_rating WHERE normalisasi.id_subkriteria='{$jeniskriteria["id"]}' ORDER BY rating_kriteria.rating  DESC limit 1");
                          $max = mysqli_fetch_array($benefit);
                          // print_r($max["nilai_rating"]);
                          // die;


                          $hnormalisasi = mysqli_query($connect, "SELECT rating_kriteria.rating as nilai_rating FROM normalisasi INNER JOIN rating_kriteria ON normalisasi.id_rating=rating_kriteria.id_rating WHERE normalisasi.id_subkriteria='{$jeniskriteria["id"]}' AND normalisasi.id_peserta='{$isi["id"]}' limit 1");
                          $data = mysqli_fetch_array($hnormalisasi);
                          $hasil = $data["nilai_rating"]/$max["nilai_rating"];

                          ?>
                          <!-- <td><?= $data["nilai_rating"]." / ".$max["nilai_rating"] ?></td> -->
                          <td><?= $hasil ?></td>
                        <?php 
                        }
                        else{
                          $cost = mysqli_query($connect, "SELECT rating_kriteria.rating as nilai_rating FROM normalisasi INNER JOIN rating_kriteria ON normalisasi.id_rating=rating_kriteria.id_rating WHERE normalisasi.id_subkriteria='{$jeniskriteria["id"]}' ORDER BY rating_kriteria.rating  ASC limit 1");
                          $min = mysqli_fetch_array($cost);

                          $hnormalisasi = mysqli_query($connect, "SELECT rating_kriteria.rating as nilai_rating FROM normalisasi INNER JOIN rating_kriteria ON normalisasi.id_rating=rating_kriteria.id_rating WHERE normalisasi.id_subkriteria='{$jeniskriteria["id"]}' AND normalisasi.id_peserta='{$isi["id"]}' limit 1");
                          $data = mysqli_fetch_array($hnormalisasi);
                          $hasil = $min["nilai_rating"]/$data["nilai_rating"];
                         ?>
                         <!-- <td><?= $min["nilai_rating"]." / ".$data["nilai_rating"]."cost" ?></td> -->
                         <td><?= $hasil ?></td>

                         <?php
                         }
                         $data = mysqli_query($connect, "SELECT id FROM hasilnormalisasi WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$jeniskriteria["id_kriteria"]}' AND id_subkriteria='{$jeniskriteria["id"]}'");
                            if($data->num_rows > 0){
                              $result= mysqli_query($connect, "UPDATE hasilnormalisasi SET nilai = '{$hasil}' WHERE id_peserta='{$isi["id"]}' AND id_kriteria='{$jeniskriteria["id_kriteria"]}' AND id_subkriteria='{$jeniskriteria["id"]}'");
                            }
                            else{
                            $sql = mysqli_query($connect, "INSERT INTO hasilnormalisasi VALUES ('','{$isi["id"]}','{$jeniskriteria["id_kriteria"]}','{$jeniskriteria["id"]}','{$hasil}')");
                            }
                        ?>
                        

                        <?php  
                        }  ?>
                         </tr>

                    <?php 
                    }
                    ?>

                    </table>
                    <div class="form-group row">
                    <a href="hasil.php">
                  <div class="col-sm-10">
                    <button type="submit" name="Tambah" value="Add" class="btn btn-primary">Hitung Nilai</button>
                  </div>
                </a>
              </div>

         <div class="card-body">
            </div>
          </div>
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>

  <?php 
  include_once 'footer.php';
   ?>