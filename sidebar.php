<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <span class="brand-text font-weight-light">Admin Page</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
        </div>
        <div class="info">
          <a href="#" class="d-block">SELAMAT DATANG !</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <!-- <a href="#" class="nav-link active">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left"></i>
              </p>
            </a> -->
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="datapeserta.php" class="nav-link <?php if($page == "Data Peserta") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="aspekkriteria.php" class="nav-link <?php if($page == "Aspek Keriteria") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Aspek Keriteria</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="jeniskriteria.php" class="nav-link <?php if($page == "Kriteria") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Subkriteria</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="subkriteria.php" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Rating</p>
                </a>
              </li> -->
              <li class="nav-item">
                <a href="normalisasi.php" class="nav-link <?php if($page == "Normalisasi") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Rating Kecocokan </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="hitungnormalisasi.php" class="nav-link <?php if($page == "Hasil Normalisasi") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Normalisasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="hasil.php" class="nav-link <?php if($page == "Perhitungan") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Perhitungan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="perangkingan.php" class="nav-link <?php if($page == "Hasil") echo "active";?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Perangkingan</p>
                </a>
              </li>
            </ul>
    <!-- /.sidebar -->
  </aside>