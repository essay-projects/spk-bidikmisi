-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Jun 2020 pada 16.33
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beasiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasilnormalisasi`
--

CREATE TABLE `hasilnormalisasi` (
  `id` int(8) NOT NULL,
  `id_peserta` int(8) NOT NULL,
  `id_kriteria` int(8) NOT NULL,
  `id_subkriteria` int(8) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasilnormalisasi`
--

INSERT INTO `hasilnormalisasi` (`id`, `id_peserta`, `id_kriteria`, `id_subkriteria`, `nilai`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 2, 0.75),
(3, 1, 1, 3, 1),
(4, 1, 1, 4, 0.25),
(5, 1, 2, 5, 0.5),
(6, 1, 2, 6, 0.5),
(7, 1, 2, 7, 0.5),
(8, 1, 2, 8, 0.5),
(9, 1, 3, 9, 0.5),
(10, 1, 3, 10, 0.33333333333333),
(11, 1, 3, 11, 0.5),
(12, 1, 3, 12, 0.5),
(13, 1, 3, 13, 1),
(14, 1, 3, 14, 0.5),
(15, 2, 1, 1, 1),
(16, 2, 1, 2, 0.5),
(17, 2, 1, 3, 0.5),
(18, 2, 1, 4, 0.33333333333333),
(19, 2, 2, 5, 0.75),
(20, 2, 2, 6, 0.75),
(21, 2, 2, 7, 1),
(22, 2, 2, 8, 0.25),
(23, 2, 3, 9, 1),
(24, 2, 3, 10, 0.5),
(25, 2, 3, 11, 1),
(26, 2, 3, 12, 1),
(27, 2, 3, 13, 1),
(28, 2, 3, 14, 0.5),
(29, 3, 1, 1, 1),
(30, 3, 1, 2, 0.5),
(31, 3, 1, 3, 0.75),
(32, 3, 1, 4, 0.25),
(33, 3, 2, 5, 1),
(34, 3, 2, 6, 1),
(35, 3, 2, 7, 1),
(36, 3, 2, 8, 0.75),
(37, 3, 3, 9, 0.5),
(38, 3, 3, 10, 0.33333333333333),
(39, 3, 3, 11, 0.5),
(40, 3, 3, 12, 1),
(41, 3, 3, 13, 1),
(42, 3, 3, 14, 0.5),
(43, 4, 1, 1, 1),
(44, 4, 1, 2, 0.75),
(45, 4, 1, 3, 0.5),
(46, 4, 1, 4, 0.33333333333333),
(47, 4, 2, 5, 0.75),
(48, 4, 2, 6, 1),
(49, 4, 2, 7, 0.25),
(50, 4, 2, 8, 0.25),
(51, 4, 3, 9, 1),
(52, 4, 3, 10, 0.5),
(53, 4, 3, 11, 0.5),
(54, 4, 3, 12, 1),
(55, 4, 3, 13, 1),
(56, 4, 3, 14, 1),
(57, 5, 1, 1, 1),
(58, 5, 1, 2, 0.5),
(59, 5, 1, 3, 0.5),
(60, 5, 1, 4, 0.33333333333333),
(61, 5, 2, 5, 0.5),
(62, 5, 2, 6, 0.5),
(63, 5, 2, 7, 1),
(64, 5, 2, 8, 0.5),
(65, 5, 3, 9, 1),
(66, 5, 3, 10, 0.5),
(67, 5, 3, 11, 0.5),
(68, 5, 3, 12, 1),
(69, 5, 3, 13, 1),
(70, 5, 3, 14, 1),
(71, 6, 1, 1, 1),
(72, 6, 1, 2, 0.75),
(73, 6, 1, 3, 0.75),
(74, 6, 1, 4, 0.25),
(75, 6, 2, 5, 0.75),
(76, 6, 2, 6, 0.75),
(77, 6, 2, 7, 1),
(78, 6, 2, 8, 1),
(79, 6, 3, 9, 1),
(80, 6, 3, 10, 0.5),
(81, 6, 3, 11, 0.5),
(82, 6, 3, 12, 1),
(83, 6, 3, 13, 1),
(84, 6, 3, 14, 1),
(85, 7, 1, 1, 1),
(86, 7, 1, 2, 0.75),
(87, 7, 1, 3, 1),
(88, 7, 1, 4, 1),
(89, 7, 2, 5, 0.5),
(90, 7, 2, 6, 1),
(91, 7, 2, 7, 0.75),
(92, 7, 2, 8, 0.25),
(93, 7, 3, 9, 1),
(94, 7, 3, 10, 0.5),
(95, 7, 3, 11, 1),
(96, 7, 3, 12, 1),
(97, 7, 3, 13, 1),
(98, 7, 3, 14, 0.5),
(99, 8, 1, 1, 1),
(100, 8, 1, 2, 1),
(101, 8, 1, 3, 0.75),
(102, 8, 1, 4, 0.33333333333333),
(103, 8, 2, 5, 0.75),
(104, 8, 2, 6, 0.75),
(105, 8, 2, 7, 0.25),
(106, 8, 2, 8, 0.5),
(107, 8, 3, 9, 1),
(108, 8, 3, 10, 0.5),
(109, 8, 3, 11, 0.5),
(110, 8, 3, 12, 1),
(111, 8, 3, 13, 1),
(112, 8, 3, 14, 1),
(113, 9, 1, 1, 1),
(114, 9, 1, 2, 1),
(115, 9, 1, 3, 0.75),
(116, 9, 1, 4, 0.33333333333333),
(117, 9, 2, 5, 0.75),
(118, 9, 2, 6, 1),
(119, 9, 2, 7, 1),
(120, 9, 2, 8, 0.25),
(121, 9, 3, 9, 1),
(122, 9, 3, 10, 0.5),
(123, 9, 3, 11, 0.5),
(124, 9, 3, 12, 1),
(125, 9, 3, 13, 1),
(126, 9, 3, 14, 1),
(127, 10, 1, 1, 1),
(128, 10, 1, 2, 0.75),
(129, 10, 1, 3, 0.5),
(130, 10, 1, 4, 0.33333333333333),
(131, 10, 2, 5, 0.75),
(132, 10, 2, 6, 0.75),
(133, 10, 2, 7, 0.75),
(134, 10, 2, 8, 0.5),
(135, 10, 3, 9, 1),
(136, 10, 3, 10, 0.5),
(137, 10, 3, 11, 1),
(138, 10, 3, 12, 1),
(139, 10, 3, 13, 1),
(140, 10, 3, 14, 0.5),
(141, 11, 1, 1, 1),
(142, 11, 1, 2, 1),
(143, 11, 1, 3, 0.75),
(144, 11, 1, 4, 0.25),
(145, 11, 2, 5, 1),
(146, 11, 2, 6, 1),
(147, 11, 2, 7, 0.75),
(148, 11, 2, 8, 0.25),
(149, 11, 3, 9, 0.5),
(150, 11, 3, 10, 0.5),
(151, 11, 3, 11, 1),
(152, 11, 3, 12, 1),
(153, 11, 3, 13, 1),
(154, 11, 3, 14, 0.5),
(155, 12, 1, 1, 1),
(156, 12, 1, 2, 0.75),
(157, 12, 1, 3, 0.75),
(158, 12, 1, 4, 0.25),
(159, 12, 2, 5, 0.5),
(160, 12, 2, 6, 0.5),
(161, 12, 2, 7, 0.25),
(162, 12, 2, 8, 0.25),
(163, 12, 3, 9, 1),
(164, 12, 3, 10, 0.5),
(165, 12, 3, 11, 1),
(166, 12, 3, 12, 1),
(167, 12, 3, 13, 1),
(168, 12, 3, 14, 0.5),
(169, 13, 1, 1, 1),
(170, 13, 1, 2, 0.5),
(171, 13, 1, 3, 0.75),
(172, 13, 1, 4, 0.33333333333333),
(173, 13, 2, 5, 0.75),
(174, 13, 2, 6, 0.75),
(175, 13, 2, 7, 1),
(176, 13, 2, 8, 0.25),
(177, 13, 3, 9, 1),
(178, 13, 3, 10, 0.5),
(179, 13, 3, 11, 0.5),
(180, 13, 3, 12, 1),
(181, 13, 3, 13, 1),
(182, 13, 3, 14, 0.5),
(183, 14, 1, 1, 1),
(184, 14, 1, 2, 0.5),
(185, 14, 1, 3, 0.75),
(186, 14, 1, 4, 0.33333333333333),
(187, 14, 2, 5, 0.5),
(188, 14, 2, 6, 0.75),
(189, 14, 2, 7, 0.25),
(190, 14, 2, 8, 0.25),
(191, 14, 3, 9, 0.5),
(192, 14, 3, 10, 0.33333333333333),
(193, 14, 3, 11, 0.5),
(194, 14, 3, 12, 1),
(195, 14, 3, 13, 1),
(196, 14, 3, 14, 1),
(197, 15, 1, 1, 1),
(198, 15, 1, 2, 0.75),
(199, 15, 1, 3, 1),
(200, 15, 1, 4, 0.25),
(201, 15, 2, 5, 0.5),
(202, 15, 2, 6, 0.75),
(203, 15, 2, 7, 0.25),
(204, 15, 2, 8, 0.25),
(205, 15, 3, 9, 1),
(206, 15, 3, 10, 1),
(207, 15, 3, 11, 1),
(208, 15, 3, 12, 1),
(209, 15, 3, 13, 1),
(210, 15, 3, 14, 1),
(211, 16, 1, 1, 1),
(212, 16, 1, 2, 0.75),
(213, 16, 1, 3, 0.75),
(214, 16, 1, 4, 0.25),
(215, 16, 2, 5, 1),
(216, 16, 2, 6, 1),
(217, 16, 2, 7, 0.25),
(218, 16, 2, 8, 0.5),
(219, 16, 3, 9, 0.5),
(220, 16, 3, 10, 0.5),
(221, 16, 3, 11, 1),
(222, 16, 3, 12, 1),
(223, 16, 3, 13, 1),
(224, 16, 3, 14, 0.5),
(225, 17, 1, 1, 1),
(226, 17, 1, 2, 0.5),
(227, 17, 1, 3, 0.75),
(228, 17, 1, 4, 0.33333333333333),
(229, 17, 2, 5, 1),
(230, 17, 2, 6, 1),
(231, 17, 2, 7, 1),
(232, 17, 2, 8, 0.5),
(233, 17, 3, 9, 0.5),
(234, 17, 3, 10, 0.5),
(235, 17, 3, 11, 1),
(236, 17, 3, 12, 1),
(237, 17, 3, 13, 1),
(238, 17, 3, 14, 0.5),
(239, 18, 1, 1, 1),
(240, 18, 1, 2, 0.25),
(241, 18, 1, 3, 0.25),
(242, 18, 1, 4, 0.33333333333333),
(243, 18, 2, 5, 0.25),
(244, 18, 2, 6, 0.5),
(245, 18, 2, 7, 1),
(246, 18, 2, 8, 0.75),
(247, 18, 3, 9, 0.5),
(248, 18, 3, 10, 0.5),
(249, 18, 3, 11, 0.5),
(250, 18, 3, 12, 1),
(251, 18, 3, 13, 1),
(252, 18, 3, 14, 0.5),
(253, 19, 1, 1, 1),
(254, 19, 1, 2, 0.5),
(255, 19, 1, 3, 0.75),
(256, 19, 1, 4, 0.33333333333333),
(257, 19, 2, 5, 1),
(258, 19, 2, 6, 1),
(259, 19, 2, 7, 0.75),
(260, 19, 2, 8, 0.25),
(261, 19, 3, 9, 1),
(262, 19, 3, 10, 1),
(263, 19, 3, 11, 1),
(264, 19, 3, 12, 1),
(265, 19, 3, 13, 1),
(266, 19, 3, 14, 0.5),
(267, 20, 1, 1, 1),
(268, 20, 1, 2, 0.75),
(269, 20, 1, 3, 0.75),
(270, 20, 1, 4, 0.25),
(271, 20, 2, 5, 0.25),
(272, 20, 2, 6, 0.5),
(273, 20, 2, 7, 0.75),
(274, 20, 2, 8, 0.25),
(275, 20, 3, 9, 1),
(276, 20, 3, 10, 1),
(277, 20, 3, 11, 1),
(278, 20, 3, 12, 1),
(279, 20, 3, 13, 1),
(280, 20, 3, 14, 0.5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitung`
--

CREATE TABLE `hitung` (
  `id` int(8) NOT NULL,
  `id_peserta` int(8) NOT NULL,
  `hasil` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hitung`
--

INSERT INTO `hitung` (`id`, `id_peserta`, `hasil`) VALUES
(1, 1, 271.66666666667),
(2, 2, 301.25),
(3, 3, 320.41666666667),
(4, 4, 295),
(5, 5, 292.5),
(6, 6, 346.25),
(7, 7, 356.25),
(8, 8, 317.5),
(9, 9, 343.75),
(10, 10, 312.5),
(11, 11, 330),
(12, 12, 276.25),
(13, 13, 302.5),
(14, 14, 264.16666666667),
(15, 15, 316.25),
(16, 16, 310),
(17, 17, 328.75),
(18, 18, 250),
(19, 19, 331.25),
(20, 20, 295);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(11) NOT NULL,
  `aspek_kriteria` varchar(35) NOT NULL,
  `bobot` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id`, `aspek_kriteria`, `bobot`) VALUES
(1, 'Aspek Orangtua', 45),
(2, 'Aspek Siswa', 35),
(3, 'Aspek Kondisi Rumah', 20);

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `normalisasi`
--

CREATE TABLE `normalisasi` (
  `id` int(25) NOT NULL,
  `id_peserta` int(25) NOT NULL,
  `id_kriteria` int(25) NOT NULL,
  `id_subkriteria` int(25) NOT NULL,
  `id_rating` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `normalisasi`
--

INSERT INTO `normalisasi` (`id`, `id_peserta`, `id_kriteria`, `id_subkriteria`, `id_rating`) VALUES
(1, 1, 1, 1, 4),
(2, 1, 1, 2, 7),
(3, 1, 1, 3, 12),
(4, 1, 1, 4, 13),
(5, 1, 2, 5, 18),
(6, 1, 2, 6, 22),
(7, 1, 2, 7, 26),
(8, 1, 2, 8, 30),
(9, 1, 3, 9, 35),
(10, 1, 3, 10, 38),
(11, 1, 3, 11, 43),
(12, 1, 3, 12, 47),
(13, 1, 3, 13, 52),
(14, 1, 3, 14, 53),
(15, 2, 1, 1, 4),
(16, 2, 1, 2, 6),
(17, 2, 1, 3, 10),
(18, 2, 1, 4, 14),
(19, 2, 2, 5, 19),
(20, 2, 2, 6, 23),
(21, 2, 2, 7, 28),
(22, 2, 2, 8, 29),
(23, 2, 3, 9, 36),
(24, 2, 3, 10, 39),
(25, 2, 3, 11, 44),
(26, 2, 3, 12, 48),
(27, 2, 3, 13, 52),
(28, 2, 3, 14, 53),
(29, 3, 1, 1, 4),
(30, 3, 1, 2, 6),
(31, 3, 1, 3, 11),
(32, 3, 1, 4, 13),
(33, 3, 2, 5, 20),
(34, 3, 2, 6, 24),
(35, 3, 2, 7, 28),
(36, 3, 2, 8, 31),
(37, 3, 3, 9, 35),
(38, 3, 3, 10, 38),
(39, 3, 3, 11, 43),
(40, 3, 3, 12, 48),
(41, 3, 3, 13, 52),
(42, 3, 3, 14, 53),
(43, 4, 1, 1, 4),
(44, 4, 1, 2, 7),
(45, 4, 1, 3, 10),
(46, 4, 1, 4, 14),
(47, 4, 2, 5, 19),
(48, 4, 2, 6, 24),
(49, 4, 2, 7, 25),
(50, 4, 2, 8, 29),
(51, 4, 3, 9, 36),
(52, 4, 3, 10, 39),
(53, 4, 3, 11, 43),
(54, 4, 3, 12, 48),
(55, 4, 3, 13, 52),
(56, 4, 3, 14, 54),
(57, 5, 1, 1, 4),
(58, 5, 1, 2, 6),
(59, 5, 1, 3, 10),
(60, 5, 1, 4, 14),
(61, 5, 2, 5, 18),
(62, 5, 2, 6, 22),
(63, 5, 2, 7, 28),
(64, 5, 2, 8, 30),
(65, 5, 3, 9, 36),
(66, 5, 3, 10, 39),
(67, 5, 3, 11, 43),
(68, 5, 3, 12, 48),
(69, 5, 3, 13, 52),
(70, 5, 3, 14, 54),
(71, 6, 1, 1, 4),
(72, 6, 1, 2, 7),
(73, 6, 1, 3, 11),
(74, 6, 1, 4, 13),
(75, 6, 2, 5, 19),
(76, 6, 2, 6, 23),
(77, 6, 2, 7, 28),
(78, 6, 2, 8, 32),
(79, 6, 3, 9, 36),
(80, 6, 3, 10, 39),
(81, 6, 3, 11, 43),
(82, 6, 3, 12, 48),
(83, 6, 3, 13, 52),
(84, 6, 3, 14, 54),
(85, 7, 1, 1, 4),
(86, 7, 1, 2, 7),
(87, 7, 1, 3, 12),
(88, 7, 1, 4, 16),
(89, 7, 2, 5, 18),
(90, 7, 2, 6, 24),
(91, 7, 2, 7, 27),
(92, 7, 2, 8, 29),
(93, 7, 3, 9, 36),
(94, 7, 3, 10, 39),
(95, 7, 3, 11, 44),
(96, 7, 3, 12, 48),
(97, 7, 3, 13, 52),
(98, 7, 3, 14, 53),
(99, 8, 1, 1, 4),
(100, 8, 1, 2, 8),
(101, 8, 1, 3, 11),
(102, 8, 1, 4, 14),
(103, 8, 2, 5, 19),
(104, 8, 2, 6, 23),
(105, 8, 2, 7, 25),
(106, 8, 2, 8, 30),
(107, 8, 3, 9, 36),
(108, 8, 3, 10, 39),
(109, 8, 3, 11, 43),
(110, 8, 3, 12, 48),
(111, 8, 3, 13, 52),
(112, 8, 3, 14, 54),
(113, 9, 1, 1, 4),
(114, 9, 1, 2, 8),
(115, 9, 1, 3, 11),
(116, 9, 1, 4, 14),
(117, 9, 2, 5, 19),
(118, 9, 2, 6, 24),
(119, 9, 2, 7, 28),
(120, 9, 2, 8, 29),
(121, 9, 3, 9, 36),
(122, 9, 3, 10, 39),
(123, 9, 3, 11, 43),
(124, 9, 3, 12, 48),
(125, 9, 3, 13, 52),
(126, 9, 3, 14, 54),
(127, 10, 1, 1, 4),
(128, 10, 1, 2, 7),
(129, 10, 1, 3, 10),
(130, 10, 1, 4, 14),
(131, 10, 2, 5, 19),
(132, 10, 2, 6, 23),
(133, 10, 2, 7, 27),
(134, 10, 2, 8, 30),
(135, 10, 3, 9, 36),
(136, 10, 3, 10, 39),
(137, 10, 3, 11, 44),
(138, 10, 3, 12, 48),
(139, 10, 3, 13, 52),
(140, 10, 3, 14, 53),
(141, 11, 1, 1, 4),
(142, 11, 1, 2, 8),
(143, 11, 1, 3, 11),
(144, 11, 1, 4, 13),
(145, 11, 2, 5, 20),
(146, 11, 2, 6, 24),
(147, 11, 2, 7, 27),
(148, 11, 2, 8, 29),
(149, 11, 3, 9, 35),
(150, 11, 3, 10, 39),
(151, 11, 3, 11, 44),
(152, 11, 3, 12, 48),
(153, 11, 3, 13, 52),
(154, 11, 3, 14, 53),
(155, 12, 1, 1, 4),
(156, 12, 1, 2, 7),
(157, 12, 1, 3, 11),
(158, 12, 1, 4, 13),
(159, 12, 2, 5, 18),
(160, 12, 2, 6, 22),
(161, 12, 2, 7, 25),
(162, 12, 2, 8, 29),
(163, 12, 3, 9, 36),
(164, 12, 3, 10, 39),
(165, 12, 3, 11, 44),
(166, 12, 3, 12, 48),
(167, 12, 3, 13, 52),
(168, 12, 3, 14, 53),
(169, 13, 1, 1, 4),
(170, 13, 1, 2, 6),
(171, 13, 1, 3, 11),
(172, 13, 1, 4, 14),
(173, 13, 2, 5, 19),
(174, 13, 2, 6, 23),
(175, 13, 2, 7, 28),
(176, 13, 2, 8, 29),
(177, 13, 3, 9, 36),
(178, 13, 3, 10, 39),
(179, 13, 3, 11, 43),
(180, 13, 3, 12, 48),
(181, 13, 3, 13, 52),
(182, 13, 3, 14, 53),
(183, 14, 1, 1, 4),
(184, 14, 1, 2, 6),
(185, 14, 1, 3, 11),
(186, 14, 1, 4, 14),
(187, 14, 2, 5, 18),
(188, 14, 2, 6, 23),
(189, 14, 2, 7, 25),
(190, 14, 2, 8, 29),
(191, 14, 3, 9, 35),
(192, 14, 3, 10, 38),
(193, 14, 3, 11, 43),
(194, 14, 3, 12, 48),
(195, 14, 3, 13, 52),
(196, 14, 3, 14, 54),
(197, 15, 1, 1, 4),
(198, 15, 1, 2, 7),
(199, 15, 1, 3, 12),
(200, 15, 1, 4, 13),
(201, 15, 2, 5, 18),
(202, 15, 2, 6, 23),
(203, 15, 2, 7, 25),
(204, 15, 2, 8, 29),
(205, 15, 3, 9, 36),
(206, 15, 3, 10, 40),
(207, 15, 3, 11, 44),
(208, 15, 3, 12, 48),
(209, 15, 3, 13, 52),
(210, 15, 3, 14, 54),
(211, 16, 1, 1, 4),
(212, 16, 1, 2, 7),
(213, 16, 1, 3, 11),
(214, 16, 1, 4, 13),
(215, 16, 2, 5, 20),
(216, 16, 2, 6, 24),
(217, 16, 2, 7, 25),
(218, 16, 2, 8, 30),
(219, 16, 3, 9, 35),
(220, 16, 3, 10, 39),
(221, 16, 3, 11, 44),
(222, 16, 3, 12, 48),
(223, 16, 3, 13, 52),
(224, 16, 3, 14, 53),
(225, 17, 1, 1, 4),
(226, 17, 1, 2, 6),
(227, 17, 1, 3, 11),
(228, 17, 1, 4, 14),
(229, 17, 2, 5, 20),
(230, 17, 2, 6, 24),
(231, 17, 2, 7, 28),
(232, 17, 2, 8, 30),
(233, 17, 3, 9, 35),
(234, 17, 3, 10, 39),
(235, 17, 3, 11, 44),
(236, 17, 3, 12, 48),
(237, 17, 3, 13, 52),
(238, 17, 3, 14, 53),
(239, 18, 1, 1, 4),
(240, 18, 1, 2, 5),
(241, 18, 1, 3, 9),
(242, 18, 1, 4, 14),
(243, 18, 2, 5, 17),
(244, 18, 2, 6, 22),
(245, 18, 2, 7, 28),
(246, 18, 2, 8, 31),
(247, 18, 3, 9, 35),
(248, 18, 3, 10, 39),
(249, 18, 3, 11, 43),
(250, 18, 3, 12, 48),
(251, 18, 3, 13, 52),
(252, 18, 3, 14, 53),
(253, 19, 1, 1, 4),
(254, 19, 1, 2, 6),
(255, 19, 1, 3, 11),
(256, 19, 1, 4, 14),
(257, 19, 2, 5, 20),
(258, 19, 2, 6, 24),
(259, 19, 2, 7, 27),
(260, 19, 2, 8, 29),
(261, 19, 3, 9, 36),
(262, 19, 3, 10, 40),
(263, 19, 3, 11, 44),
(264, 19, 3, 12, 48),
(265, 19, 3, 13, 52),
(266, 19, 3, 14, 53),
(267, 20, 1, 1, 4),
(268, 20, 1, 2, 7),
(269, 20, 1, 3, 11),
(270, 20, 1, 4, 13),
(271, 20, 2, 5, 17),
(272, 20, 2, 6, 22),
(273, 20, 2, 7, 27),
(274, 20, 2, 8, 29),
(275, 20, 3, 9, 36),
(276, 20, 3, 10, 40),
(277, 20, 3, 11, 44),
(278, 20, 3, 12, 48),
(279, 20, 3, 13, 52),
(280, 20, 3, 14, 53);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peserta`
--

CREATE TABLE `peserta` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `penghasilan` int(15) NOT NULL,
  `pendidikan` varchar(5) NOT NULL,
  `pekerjaan` varchar(25) NOT NULL,
  `tanggungan` int(2) NOT NULL,
  `nilai_un` varchar(8) NOT NULL,
  `nilai_ijazah` varchar(8) NOT NULL,
  `p_akademik` int(8) NOT NULL,
  `p_nonakademik` varchar(25) NOT NULL,
  `l_rumah` int(10) NOT NULL,
  `l_tanah` int(10) NOT NULL,
  `listrik` int(10) NOT NULL,
  `pdam` int(10) NOT NULL,
  `pbb` int(10) NOT NULL,
  `status_rumah` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peserta`
--

INSERT INTO `peserta` (`id`, `nama`, `penghasilan`, `pendidikan`, `pekerjaan`, `tanggungan`, `nilai_un`, `nilai_ijazah`, `p_akademik`, `p_nonakademik`, `l_rumah`, `l_tanah`, `listrik`, `pdam`, `pbb`, `status_rumah`) VALUES
(1, 'Mhs1', 1300000, 'SLTP', 'Petani', 4, '62.13', '70', 3, 'Tk Provinsi', 80, 100, 180000, 125000, 98000, 'Milik Sendiri'),
(2, 'Mhs2', 500000, 'SLTA', 'Pedagang', 5, '72.5', '79.45', 1, 'Tk Kabupaten', 50, 75, 90000, 85000, 70000, 'Milik Sendiri'),
(3, 'Mhs3', 1200000, 'SLTA', 'Buruh harian lepas', 4, '87.75', '89.53', 1, 'Tk Nasional', 85, 100, 125000, 95000, 90000, 'Milik Sendiri'),
(4, 'Mhs4', 900000, 'SLTP', 'Pedagang', 5, '77', '82.64', 10, 'Tk Kabupaten', 45, 75, 100000, 85000, 75000, 'Menumpang'),
(5, 'Mhs5', 2000000, 'SLTA', 'Pedagang', 5, '60.25', '68.66', 1, 'Tk Provinsi', 40, 70, 115000, 85000, 85000, 'Sewa'),
(6, 'Mhs6', 500000, 'SLTP', 'Buruh harian lepas', 2, '73.5', '79.81', 1, 'Tk Internasional', 43, 55, 100000, 70000, 80000, 'Menumpang'),
(7, 'Mhs7', 800000, 'SLTP', 'Petani', 9, '65.5', '80.34', 2, 'Tk Kabupaten', 45, 60, 85000, 60000, 78000, 'Milik Sendiri'),
(8, 'Mhs8', 500000, 'SD', 'Buruh bangunan', 5, '73', '78.25', 7, 'Tk Provinsi', 40, 55, 105000, 65000, 69000, 'Menumpang'),
(9, 'Mhs9', 300000, 'SD', 'Buruh harian lepas', 5, '79', '85.54', 1, 'Tk Kabupaten', 36, 72, 110000, 78000, 87000, 'Menumpang'),
(10, 'Mhs10', 500000, 'SLTP', 'Karyawan swasta', 5, '76.75', '79.25', 2, 'Tk Provinsi', 40, 75, 95000, 50000, 70000, 'Milik Sendiri'),
(11, 'Mhs11', 600000, 'SD', 'Pekerja Buruh', 4, '82.63', '86.75', 2, 'Tk Kabupaten', 65, 70, 65000, 45000, 56000, 'Milik Sendiri'),
(12, 'Mhs12', 500000, 'SLTP', 'Pekerja Buruh', 3, '61.63', '65.5', 4, 'Tk Kabupaten', 45, 55, 95000, 35000, 55000, 'Milik Sendiri'),
(13, 'Mhs13', 800000, 'SLTA', 'Pekerja Buruh', 6, '72.42', '75', 1, 'Tk Kabupaten', 45, 68, 100000, 55000, 50000, 'Milik Sendiri'),
(14, 'Mhs14', 1500000, 'SLTA', 'Buruh harian lepas', 6, '69.5', '78.89', 9, 'Tk Kabupaten', 90, 105, 150000, 65000, 90000, 'Menumpang'),
(15, 'Mhs15', 800000, 'SLTP', 'Petani', 4, '64', '70.75', 7, 'Tk Kabupaten', 36, 45, 85000, 45000, 50000, 'Menumpang'),
(16, 'Mhs16', 780000, 'SLTP', 'Pekerja Buruh', 4, '86.83', '82.1', 10, 'Tk Provinsi', 70, 75, 65000, 0, 35000, 'Milik Sendiri'),
(17, 'Mhs17', 600000, 'SLTA', 'Pekerja Buruh', 5, '83.63', '85.88', 1, 'Tk Provinsi', 82, 90, 79000, 25000, 54000, 'Milik Sendiri'),
(18, 'Mhs18', 2000000, 'S1', 'Guru', 5, '53', '65.78', 1, 'Tk Nasional', 55, 70, 130000, 75000, 65000, 'Milik Sendiri'),
(19, 'Mhs19', 984200, 'SLTA', 'Buruh harian lepas', 5, '90.3', '85.65', 2, 'Tk Kabupaten', 36, 42, 95000, 0, 50000, 'Milik Sendiri'),
(20, 'Mhs20', 750000, 'SLTP', 'Tukang', 4, '52.88', '60.45', 2, 'Tk Kabupaten', 36, 40, 75000, 0, 45000, 'Milik Sendiri');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating_kriteria`
--

CREATE TABLE `rating_kriteria` (
  `id_rating` int(11) NOT NULL,
  `id_kriteria` int(25) NOT NULL,
  `id_subkriteria` int(11) NOT NULL,
  `nama_rating` varchar(255) NOT NULL,
  `min` double NOT NULL,
  `max` double NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating_kriteria`
--

INSERT INTO `rating_kriteria` (`id_rating`, `id_kriteria`, `id_subkriteria`, `nama_rating`, `min`, `max`, `rating`) VALUES
(1, 1, 1, '> 6.000.000', 6000000, 10000000, 4),
(2, 1, 1, '4.501.000 - 6.000.000', 4501000, 6000000, 3),
(3, 1, 1, '3.001.000 - 4.500.000', 3001000, 4500000, 2),
(4, 1, 1, '< 3.000.000', 0, 3000000, 1),
(5, 1, 2, 'S1 / S2 / S3', 1, 1, 1),
(6, 1, 2, 'SLTA', 2, 2, 2),
(7, 1, 2, 'SLTP', 3, 3, 3),
(8, 1, 2, 'SD / TT SD', 4, 4, 4),
(9, 1, 3, 'PNS / TNI / Polri / Guru', 1, 1, 1),
(10, 1, 3, 'Wiraswasta / Karyawan swasta / Pedagang', 2, 2, 2),
(11, 1, 3, 'Tukang / Buruh harian lepas / Buruh bangunan / Pekerja Buruh', 3, 3, 3),
(12, 1, 3, 'Buruh Tani / Petani', 4, 4, 4),
(13, 1, 4, '< 5 orang', 0, 4, 4),
(14, 1, 4, '5 - 6 orang', 5, 6, 3),
(15, 1, 4, '7 - 8 orang', 7, 8, 2),
(16, 1, 4, '> 8 orang', 9, 10, 1),
(17, 2, 5, '< 60.00', 0, 59.99, 1),
(18, 2, 5, '60.00 - 70.00', 60, 70, 2),
(19, 2, 5, '70.01 - 80.00', 70.01, 80, 3),
(20, 2, 5, '> 80.00', 80.01, 100, 4),
(21, 2, 6, '< 60.00', 0, 59.99, 1),
(22, 2, 6, '60.00 - 70.00', 60, 70, 2),
(23, 2, 6, '70.01 - 80.00', 70.01, 80, 3),
(24, 2, 6, '> 80.00', 80.01, 100, 4),
(25, 2, 7, 'Rangking 4 - 10', 4, 10, 1),
(26, 2, 7, 'Rangking 3', 3, 3, 2),
(27, 2, 7, 'Rangking 2', 2, 2, 3),
(28, 2, 7, 'Rangking 1', 1, 1, 4),
(29, 2, 8, 'Tk Kabupaten', 1, 1, 1),
(30, 2, 8, 'Tk Provinsi', 2, 2, 2),
(31, 2, 8, 'Tk Nasional', 3, 3, 3),
(32, 2, 8, 'Tk Internasional', 4, 4, 4),
(33, 3, 9, '> 200m2', 201, 500, 4),
(34, 3, 9, '100 - 200m2', 100, 200, 3),
(35, 3, 9, '51 - 99m2', 51, 99, 2),
(36, 3, 9, '10 - 50 m2', 10, 50, 1),
(37, 3, 10, '> 200m2', 201, 500, 4),
(38, 3, 10, '100 - 200m2', 100, 200, 3),
(39, 3, 10, '51 - 99m2', 51, 99, 2),
(40, 3, 10, '10 - 50 m2', 10, 50, 1),
(41, 3, 11, '> 300.000', 301000, 800000, 4),
(42, 3, 11, '201.000 - 300.000', 201000, 300000, 3),
(43, 3, 11, '100.000 - 200.000', 100000, 200000, 2),
(44, 3, 11, '< 100.000', 0, 99000, 1),
(45, 3, 12, '> 300.000', 301000, 800000, 4),
(46, 3, 12, '201.000 - 300.000', 201000, 300000, 3),
(47, 3, 12, '100.000 - 200.000', 100000, 200000, 2),
(48, 3, 12, '< 100.000', 0, 99000, 1),
(49, 3, 13, '>300.000', 301000, 800000, 4),
(50, 3, 13, '201.000 - 300.000', 201000, 300000, 3),
(51, 3, 13, '100.000 - 200.000', 100000, 200000, 2),
(52, 3, 13, '< 100.000', 0, 99000, 1),
(53, 3, 14, 'Milik Sendiri', 1, 1, 1),
(54, 3, 14, 'Menumpang / Kontrak / Sewa', 2, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkriteria`
--

CREATE TABLE `subkriteria` (
  `id` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `sub_kriteria` varchar(25) NOT NULL,
  `jenis_subkriteria` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `subkriteria`
--

INSERT INTO `subkriteria` (`id`, `id_kriteria`, `sub_kriteria`, `jenis_subkriteria`) VALUES
(1, 1, 'Penghasilan', 'Cost'),
(2, 1, 'Pendidikan', 'Benefit'),
(3, 1, 'Pekerjaan', 'Benefit'),
(4, 1, 'Jumlah Tanggungan', 'Cost'),
(5, 2, 'Rata-Rata Nilai UN', 'Benefit'),
(6, 2, 'Rata-Rata Nilai Ijazah', 'Benefit'),
(7, 2, 'Prestasi Akademik', 'Benefit'),
(8, 2, 'Prestasi Non-Akademik', 'Benefit'),
(9, 3, 'Luas Rumah', 'Cost'),
(10, 3, 'Luas Tanah', 'Cost'),
(11, 3, 'Rekening Listrik/Bln', 'Cost'),
(12, 3, 'PDAM', 'Cost'),
(13, 3, 'PBB/Thn', 'Cost'),
(14, 3, 'Kepemilikan Rumah', 'Benefit');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `hasilnormalisasi`
--
ALTER TABLE `hasilnormalisasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_peserta` (`id_peserta`);

--
-- Indeks untuk tabel `hitung`
--
ALTER TABLE `hitung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hitung_ibfk_1` (`id_peserta`);

--
-- Indeks untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kriteria` (`id_kriteria`),
  ADD KEY `id_subkriteria` (`id_subkriteria`),
  ADD KEY `id_rating` (`id_rating`),
  ADD KEY `normalisasi_ibfk_4` (`id_peserta`);

--
-- Indeks untuk tabel `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rating_kriteria`
--
ALTER TABLE `rating_kriteria`
  ADD PRIMARY KEY (`id_rating`),
  ADD KEY `rating_kriteria_ibfk_1` (`id_subkriteria`),
  ADD KEY `id_kriteria` (`id_kriteria`);

--
-- Indeks untuk tabel `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kriteria` (`id_kriteria`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `hasilnormalisasi`
--
ALTER TABLE `hasilnormalisasi`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT untuk tabel `hitung`
--
ALTER TABLE `hitung`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT untuk tabel `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `rating_kriteria`
--
ALTER TABLE `rating_kriteria`
  MODIFY `id_rating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT untuk tabel `subkriteria`
--
ALTER TABLE `subkriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hasilnormalisasi`
--
ALTER TABLE `hasilnormalisasi`
  ADD CONSTRAINT `hasilnormalisasi_ibfk_1` FOREIGN KEY (`id_peserta`) REFERENCES `peserta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `hitung`
--
ALTER TABLE `hitung`
  ADD CONSTRAINT `hitung_ibfk_1` FOREIGN KEY (`id_peserta`) REFERENCES `peserta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  ADD CONSTRAINT `normalisasi_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`),
  ADD CONSTRAINT `normalisasi_ibfk_2` FOREIGN KEY (`id_subkriteria`) REFERENCES `subkriteria` (`id`),
  ADD CONSTRAINT `normalisasi_ibfk_3` FOREIGN KEY (`id_rating`) REFERENCES `rating_kriteria` (`id_rating`),
  ADD CONSTRAINT `normalisasi_ibfk_4` FOREIGN KEY (`id_peserta`) REFERENCES `peserta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `rating_kriteria`
--
ALTER TABLE `rating_kriteria`
  ADD CONSTRAINT `rating_kriteria_ibfk_1` FOREIGN KEY (`id_subkriteria`) REFERENCES `subkriteria` (`id`),
  ADD CONSTRAINT `rating_kriteria_ibfk_2` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`);

--
-- Ketidakleluasaan untuk tabel `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD CONSTRAINT `subkriteria_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
